import React from 'react';


const NotFound = () => {
    return (
        <div>
            <h2>Not Found</h2>
            <p>The page you searching is not exists.</p>
        </div>
    )
};

export default NotFound