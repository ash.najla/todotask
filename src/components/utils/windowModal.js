import React from 'react';
import { Modal, Button } from "antd";
import { UserAddForm } from '../forms/userForm';
import { TodoAddForm } from '../forms/todoForm';

export class ModalWindow extends React.Component {
    render() {
        const { visible, onCancel, onCreate, option, confirmLoading, wrappedComponentRef } = this.props;

        return (
            <Modal
                title={'Add New ' + option}
                visible={visible}
                confirmLoading={confirmLoading}
                onCancel={onCancel}
                onOk={onCreate}
                footer={[
                    <Button key="back" onClick={onCancel}>
                        Cancel
                    </Button>,
                    <Button key="submit" type="primary" loading={confirmLoading} onClick={onCreate}>
                        Save
                    </Button>,
                ]}
            >
                {option === "User" ?
                    <UserAddForm
                        wrappedComponentRef={wrappedComponentRef}
                        handleSubmit={onCreate}
                    />
                    :
                    <TodoAddForm
                        wrappedComponentRef={wrappedComponentRef}
                        handleSubmit={onCreate}
                    />
                }
            </Modal>
        )
    }
}